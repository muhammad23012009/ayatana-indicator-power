Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ayatana-indicator-power
Upstream-Contact: Ayatana Indicator Power
Source: https://github.com/AyatanaIndicators/ayatana-indicator-power

Files: .build.yml
 .travis.yml
 AUTHORS
 CMakeLists.txt
 ChangeLog
 INSTALL.md
 NEWS
 NEWS.Canonical
 README.md
 cmake/GdbusCodegen.cmake
 data/CMakeLists.txt
 data/ayatana-indicator-power.desktop.in
 data/ayatana-indicator-power.service.in
 data/org.ayatana.indicator.power
 data/org.ayatana.indicator.power.Battery.xml
 data/org.ayatana.indicator.power.Testing.xml
 data/org.ayatana.indicator.power.gschema.xml
 po/CMakeLists.txt
 po/LINGUAS
 po/POTFILES.in
 src/CMakeLists.txt
 src/com.lomiri.Repowerd.xml
 tests/CMakeLists.txt
 tests/manual
Copyright: 2011-2016, Canonical Ltd.
License: GPL-3
Comment:
 Assuming same license and copyright holdership as found in the
 project files containing a license header.

Files: po/*.po
 po/ayatana-indicator-power.pot
Copyright: 2017, THE PACKAGE'S COPYRIGHT HOLDER
License: GPL-3

Files: src/brightness.h
 src/datafiles.c
 src/datafiles.h
 src/dbus-shared.h
 src/device-provider-mock.c
 src/device-provider-mock.h
 src/device-provider-upower.c
 src/device-provider-upower.h
 src/device-provider.c
 src/device-provider.h
 src/device.h
 src/main.c
 src/notifier.h
 src/service.h
 src/testing.c
 src/testing.h
 tests/glib-fixture.h
 tests/test-dbus-listener.cc
 tests/test-service.cc
Copyright: 2012, Canonical Ltd.
  2013, Canonical Ltd.
  2013-2016, Canonical Ltd.
  2014, Canonical Ltd.
  2014-2016, Canonical Ltd.
  2016, Canonical Ltd.
License: GPL-3

Files: src/brightness.c
 src/device.c
 src/notifier.c
 src/service.c
 tests/test-device.cc
 tests/test-notify.cc
Copyright: 2012, Canonical Ltd.
  2012-2016, Canonical Ltd.
  2013-2016, Canonical Ltd.
  2014, Canonical Ltd.
  2014-2016, Canonical Ltd.
  2021, Robert Tari
  2021-2022, Robert Tari
  2022, Robert Tari
License: GPL-3

Files: update-po.sh
 update-pot.sh
Copyright: 2017, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-3

Files: src/flashlight.c
 src/flashlight.h
Copyright: 2017, The UBports project
License: GPL-3

Files: src/utils.c
Copyright: 2013, Canonical Ltd.
  2018, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2021, Robert Tari <robert@tari.in>
License: GPL-3

Files: src/utils.h
Copyright: 2018, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
  2021, Robert Tari <robert@tari.in>
License: GPL-3

Files: debian/*
Copyright: 2011 Ken VanDine <ken.vandine@canonical.com>
           2015-2022, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
